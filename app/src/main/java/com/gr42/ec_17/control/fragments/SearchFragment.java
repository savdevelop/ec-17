package com.gr42.ec_17.control.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.control.UserController;

public class SearchFragment extends Fragment {

    private String[] searches = null;
    private ListView listView;
    private SearchView searchView;
    private UserController userController;
    private View mainView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_search, container, false);

        listView = mainView.findViewById(R.id.listView);
        searchView = mainView.findViewById(R.id.searchView);
        userController = UserController.getInstance();

        try {
            searches = userController.getLatestSearchStrings();
            if(searches != null) {
                Adapter adapter = new Adapter(mainView.getContext(), R.layout.row_latest_search, searches);

                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String query = searches[position];
                        Log.i("SEARCH", query);
                        goToSearchResultFragment(query);
                    }
                });
            }
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), getString(R.string.serverError) + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                goToSearchResultFragment(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return mainView;
    }

    class Adapter extends ArrayAdapter<String> {
        Context context;
        String[] searches;
        int layoutID;

        Adapter(Context context, int layoutID, String[] searches) {
            super(context, layoutID, searches);
            this.searches = searches;
            this.context = context;
            this.layoutID = layoutID;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService((Context.LAYOUT_INFLATER_SERVICE));
            convertView = layoutInflater.inflate(layoutID, null);
            TextView tv = (TextView) convertView.findViewById(R.id.textRow);

            tv.setText(searches[position]);
            return convertView;
        }
    }

    private void goToSearchResultFragment(String query) {
        try{
            userController.addSearchString(query);
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), getString(R.string.serverError) + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        Bundle arg = new Bundle();
        arg.putString(SearchResultFragment.SEARCH_KEY, query);
        ((MainActivity) getActivity()).replaceFragments(SearchResultFragment.class, arg, MainActivity.SEARCH_RESULT_TAG, true);
    }

}
