package com.gr42.ec_17.control.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.gr42.ec_17.App;
import com.gr42.ec_17.DBException;
import com.gr42.ec_17.PaypalActivity;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.control.fragments.AddressesFragment;
import com.gr42.ec_17.control.fragments.CartFragment;
import com.gr42.ec_17.control.fragments.CategoriesFragment;
import com.gr42.ec_17.control.fragments.HomepageFragment;
import com.gr42.ec_17.control.fragments.OrdersFragment;
import com.gr42.ec_17.control.fragments.ProfileFragment;
import com.gr42.ec_17.control.fragments.SearchFragment;
import com.gr42.ec_17.entity.Address;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;

    public static final String HOME_TAG = "0";
    public static final String PROFILE_TAG = "1";
    public static final String ORDERS_TAG = "2";
    public static final String ADDRESSES_TAG = "3";
    public static final String CATEGORY_TAG = "4";
    public static final String SEARCH_TAG = "5";
    public static final String CART_TAG = "6";
    public static final String SEARCH_RESULT_TAG = "7";
    public static final String PRODUCT_TAG = "8";
    public static final String NEWEDITADRR_TAG = "9";
    public static final String ORDERSUM_TAG = "10";
    private static final int[] ids = {R.id.nav_home, R.id.nav_profile, R.id.nav_orders, R.id.nav_addresses, R.id.nav_category};
    private NavigationView navigationView;
    private Menu menu;
    public static final int PAYMENT_RESULT = 17;
    private Address orderAddress;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread initThread = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    Toolbar toolbar = findViewById(R.id.toolbar);
                    setSupportActionBar(toolbar);
                    progressBar = findViewById(R.id.progress_bar);

                    drawerLayout = findViewById(R.id.drawer_layout);
                    navigationView = findViewById(R.id.nav_view);

                    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getParent(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                    drawerLayout.addDrawerListener(toggle);
                    toggle.syncState();

                    navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                            switch (menuItem.getItemId()) {
                                case R.id.nav_home:
                                    replaceFragments(HomepageFragment.class, null, HOME_TAG, true);
                                    break;
                                case R.id.nav_profile:
                                    replaceFragments(ProfileFragment.class, null, PROFILE_TAG, true);
                                    break;
                                case R.id.nav_orders:
                                    replaceFragments(OrdersFragment.class, null, ORDERS_TAG, true);
                                    break;
                                case R.id.nav_addresses:
                                    replaceFragments(AddressesFragment.class, null, ADDRESSES_TAG, true);
                                    break;
                                case R.id.nav_category:
                                    replaceFragments(CategoriesFragment.class, null, CATEGORY_TAG, true);
                                    break;
                                case R.id.nav_logout:
                                    logout();
                                    break;
                            }

                            drawerLayout.closeDrawer(GravityCompat.START);
                            return true;
                        }
                    });
                    sleep(10);
                } catch (Exception e) {
                } finally {
                    replaceFragments(HomepageFragment.class, null, HOME_TAG, false);
                }
            }
        };
        initThread.start();
    }

    private void logout() {
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Do you really want to logout?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        UserController.getInstance().logout();
                        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search:
                replaceFragments(SearchFragment.class, null, SEARCH_TAG, true);
                break;
            case R.id.cart:
                replaceFragments(CartFragment.class, null, CART_TAG, true);
                break;
        }
        return true;
    }

    public void replaceFragments(final Class fragmentClass, final Bundle args, @NonNull final String tag, final boolean addToBackStack) {
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                if (!isCurrentFragment(tag)) {
                    Fragment fragment = null;
                    FragmentTransaction fragmentTransaction = null;
                    try {
                        fragment = (Fragment) fragmentClass.newInstance();
                        if (args != null) {
                            fragment.setArguments(args);
                        }
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, fragment, tag);
                        if (addToBackStack)
                            fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        int index = Integer.parseInt(tag);
                        if (index < ids.length)
                            navigationView.setCheckedItem(ids[index]);
                        else {
                            if (navigationView.getCheckedItem() != null)
                                navigationView.getCheckedItem().setChecked(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        mPendingRunnable.run();
    }

    public void refreshCurrentFragment(String tag) {
        if (isCurrentFragment(tag)) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.detach(fragment);
            fragmentTransaction.attach(fragment);
            fragmentTransaction.commit();
        }
    }

    private boolean isCurrentFragment(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null && fragment.isVisible()) {
            return true;
        } else {
            return false;
        }
    }

    public void goToPaypal(float totalCheckout, Address address) {
        orderAddress = address;
        Bundle args = new Bundle();
        args.putFloat(PaypalActivity.AMOUNT_TO_PAY, totalCheckout);
        Intent intent = new Intent(this, PaypalActivity.class);
        intent.putExtras(args);
        startActivityForResult(intent, PAYMENT_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAYMENT_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    UserController.getInstance().addOrder(orderAddress);
                    Toast.makeText(this, "Thanks for buying from our store!", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(getIntent());
                } catch (DBException e) {
                    e.printStackTrace();
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            }
            orderAddress = null;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void disableProgressBar() {
        progressBar.setVisibility(View.GONE);
    }
}
