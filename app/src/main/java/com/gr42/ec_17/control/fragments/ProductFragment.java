package com.gr42.ec_17.control.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.SwipeAdapter;
import com.gr42.ec_17.control.BackendInterface;
import com.gr42.ec_17.control.ProductController;
import com.gr42.ec_17.entity.Product;
import com.gr42.ec_17.entity.Review;

public class ProductFragment extends Fragment implements View.OnClickListener {
    public static final String PRODUCT_KEY = "PRODUCT_ID";
    private View mainView;
    private ProductController productController;
    private Product product;
    private TextView category, title, description, price, reviewTitle, reviewDescription;
    private Spinner spinner;
    private ImageView[] productStars, newReviewStars;
    private ViewPager imagePager;
    private Button addToCart, submitReview;
    private ViewGroup reviewsLayout;
    int rating;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_product, container, false);
        productController = ProductController.getInstance();
        initComponents();

        String productID = null;
        Bundle extras = getArguments();
        if (extras != null) {
            if (extras.containsKey(PRODUCT_KEY)) {
                productID = extras.getString(PRODUCT_KEY);
                Log.i("GENTILE", productID);
            }
        }

        try {
            product = productController.getProduct(productID);

            if (product != null) {
                SwipeAdapter swipeAdapter = new SwipeAdapter(mainView.getContext(), product.getImageURLs(), R.layout.item_image);
                imagePager.setAdapter(swipeAdapter);

                category.setText(product.getCategory().getTitle());
                title.setText(product.getTitle());
                description.setText(product.getDescription());
                price.setText(getString(R.string.currency) + Float.toString(product.getPrice()));

                //spinner setup
                String[] values = new String[product.getQuantityAvailable()];
                for (int i = 0; i < product.getQuantityAvailable(); i++) {
                    values[i] = Integer.toString(i + 1);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainView.getContext(), android.R.layout.simple_spinner_item, values);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                //productStars setup
                float fRating = product.getAvgRating();
                int iRating = (int) fRating;
                turnOnStars(productStars, iRating);
                if (iRating != Math.round(fRating)) {
                    productStars[iRating].setImageResource(R.drawable.ic_half_star_24dp);
                }


                Review[] reviews = product.getReviews();
                if (reviews != null) {
                    for (int i = 0; i < reviews.length; i++) {
                        addRow(reviewsLayout, reviews[i]);
                        if (i != reviews.length - 1)
                            addDivider(reviewsLayout);
                    }
                }
            }

        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return mainView;
    }

    private void initComponents() {
        category = mainView.findViewById(R.id.product_category);
        title = mainView.findViewById(R.id.product_title);
        description = mainView.findViewById(R.id.product_description);
        price = mainView.findViewById(R.id.product_price);
        spinner = mainView.findViewById(R.id.quantity_spinner);
        addToCart = mainView.findViewById(R.id.addToCart);
        submitReview = mainView.findViewById(R.id.submit);
        reviewTitle = mainView.findViewById(R.id.review_title);
        reviewDescription = mainView.findViewById(R.id.review_description);
        imagePager = mainView.findViewById(R.id.image_pager);
        reviewsLayout = mainView.findViewById(R.id.reviews_layout);
        productStars = new ImageView[]{mainView.findViewById(R.id.product_star1), mainView.findViewById(R.id.product_star2), mainView.findViewById(R.id.product_star3), mainView.findViewById(R.id.product_star4), mainView.findViewById(R.id.product_star5)};
        newReviewStars = new ImageView[]{mainView.findViewById(R.id.rating_star1), mainView.findViewById(R.id.rating_star2), mainView.findViewById(R.id.rating_star3), mainView.findViewById(R.id.rating_star4), mainView.findViewById(R.id.rating_star5)};

        for(ImageView i : newReviewStars){
            i.setOnClickListener(this);
        }
        addToCart.setOnClickListener(this);
        submitReview.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int value = 0;
        switch (v.getId()) {
            case R.id.rating_star5:
                value++;
            case R.id.rating_star4:
                value++;
            case R.id.rating_star3:
                value++;
            case R.id.rating_star2:
                value++;
            case R.id.rating_star1:
                value++;
                turnOnStars(newReviewStars, value);
                rating = value;
                break;
            case R.id.addToCart:
                try {
                    int quant = Integer.parseInt(spinner.getSelectedItem().toString());
                    productController.addProductToCart(product.getProductID(), quant);
                    Toast.makeText(mainView.getContext(), getString(R.string.product_added), Toast.LENGTH_SHORT).show();
                } catch (DBException e) {
                    String error;
                    if(e.getMessage() == BackendInterface.PRODUCT_ERROR)
                        error = getString(R.string.product_already);
                    else
                        error = e.getMessage();
                    Toast.makeText(mainView.getContext(), error, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.submit:
                try {
                    productController.addReview(reviewTitle.getText().toString(), reviewDescription.getText().toString(), rating);

                    Toast.makeText(mainView.getContext(), "Submitted", Toast.LENGTH_SHORT).show();
                } catch (DBException e) {
                    Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void turnOnStars(ImageView[] stars, int value) {
        for (int i = 0; i < stars.length; i++) {
            if (i < value)
                stars[i].setImageResource(R.drawable.ic_full_star_24dp);
            else
                stars[i].setImageResource(R.drawable.ic_empty_star_24dp);
        }
    }

    private void addRow(ViewGroup mLinearLayout, Review review) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.row_review, mLinearLayout, false);

        TextView userName, tit, desc;
        ImageView[] reviewStars = new ImageView[]{row.findViewById(R.id.row_star1), row.findViewById(R.id.row_star2), row.findViewById(R.id.row_star3), row.findViewById(R.id.row_star4), row.findViewById(R.id.row_star5)};

        userName = row.findViewById(R.id.row_user);
        tit = row.findViewById(R.id.row_review_title);
        desc = row.findViewById(R.id.row_description);

        userName.setText(review.getUserName());
        tit.setText(review.getTitle());
        desc.setText(review.getDescription());

        turnOnStars(reviewStars, review.getRating());

        mLinearLayout.addView(row);
    }

    private void addDivider(ViewGroup mLinearLayout) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.divider, mLinearLayout, false);
        mLinearLayout.addView(row);
    }

}
