package com.gr42.ec_17.control.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.BackendInterface;
import com.gr42.ec_17.control.ProductController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.Category;
import com.gr42.ec_17.entity.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SearchResultFragment extends Fragment implements View.OnClickListener {

    public static final String SEARCH_KEY = "QUERY_STRING";
    public static final String CATEGORY_KEY = "CATEGORY_ID";
    private String query;
    private ListView listView;
    private View mainView;
    private TextView results;
    private SearchView searchView;
    private Button filters;
    private Product[] products = null;
    Category[] categories = null;
    private ProductController productController;
    private String name = null, categoryId;
    private Spinner orderBy, categoryFilter;
    private int check = 0, res = 0;
    private Adapter adapter;
    private ImageView[] filterStars;
    private Dialog dialog;
    private EditText fromFilter, toFilter;
    private double from, to;
    private int ratingFilter = 0;
    private Button reset;

    //filters
    HashMap<String, Double> greater = new HashMap<>();
    HashMap<String, Double> less = new HashMap<>();
    HashMap<String, Boolean> order = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_search_result, container, false);
        productController = ProductController.getInstance();
        initComponents();

        Bundle extras = getArguments();
        if (extras != null) {
            if (extras.containsKey(SEARCH_KEY)) {
                name = extras.getString(SEARCH_KEY);
                searchView.setQuery(name, false);
                this.query = extras.getString(SEARCH_KEY);
            } else if (extras.containsKey(CATEGORY_KEY)) {
                categoryId = extras.getString(CATEGORY_KEY);
            }
        }

        //spinner setup
        String[] values = {"", getString(R.string.order_by_price_asc), getString(R.string.order_by_price_desc), getString(R.string.order_by_rating_asc), getString(R.string.order_by_rating_desc)};
        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(mainView.getContext(), android.R.layout.simple_spinner_item, values);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderBy.setAdapter(adapterSpinner);
        orderBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++check > 1) {
                    listView.setAdapter(null);
                }
                if (position != 0) {
                    String key = null;
                    boolean value = true;
                    if (order.size() != 0)
                        order.clear();
                    switch (position) {
                        case 1:
                            key = BackendInterface.PRICE;
                            value = BackendInterface.ASCENDING;
                            break;
                        case 2:
                            key = BackendInterface.PRICE;
                            value = BackendInterface.DESCENDING;
                            break;
                        case 3:
                            key = BackendInterface.RATING;
                            value = BackendInterface.ASCENDING;
                            break;
                        case 4:
                            key = BackendInterface.RATING;
                            value = BackendInterface.DESCENDING;
                            break;
                    }
                    order.put(key, value);
                }
                doQuery();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                name = query;
                doQuery();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterPopup();
            }
        });

        return mainView;
    }

    private void initComponents() {
        listView = mainView.findViewById(R.id.listView);
        results = mainView.findViewById(R.id.results);
        searchView = mainView.findViewById(R.id.searchView);
        orderBy = mainView.findViewById(R.id.order_by);
        filters = mainView.findViewById(R.id.filters);
    }

    private void doQuery() {
        try {
            products = productController.getProducts(name, categoryId, greater, less, order, 0);

            if (products != null) {
                adapter = new Adapter(mainView.getContext(), R.layout.row_search_product, products);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        goToProductFragment(products[position].getProductID());
                    }
                });
                res = products.length;

            } else {
                listView.setAdapter(null);
                res = 0;
                Toast.makeText(mainView.getContext(), getString(R.string.no_products), Toast.LENGTH_SHORT).show();
            }
            results.setText(getString(R.string.results) + res);
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), getString(R.string.product_not_found), Toast.LENGTH_SHORT).show();
        }
    }

    private void showFilterPopup() {
        if (dialog == null) {
            dialog = new Dialog(mainView.getContext());
            dialog.setContentView(R.layout.popup_filters);
            fromFilter = dialog.findViewById(R.id.from);
            toFilter = dialog.findViewById(R.id.to);
            filterStars = new ImageView[]{dialog.findViewById(R.id.rating_star1), dialog.findViewById(R.id.rating_star2), dialog.findViewById(R.id.rating_star3), dialog.findViewById(R.id.rating_star4), dialog.findViewById(R.id.rating_star5)};
            Button saveFilters = dialog.findViewById(R.id.save_btn);
            categoryFilter = dialog.findViewById(R.id.category_spinner);
            reset = dialog.findViewById(R.id.reset);
            for (ImageView i : filterStars) {
                i.setOnClickListener(this);
            }


            //category filter setup
            try {
                final Category[] categories = productController.getCategories();
                if (categories != null && categories.length > 0) {
                    String[] cats = new String[categories.length + 1];
                    int i = 1;
                    cats[0] = getString(R.string.select_category);
                    for (Category c : categories) {
                        cats[i] = c.getTitle();
                        i++;
                    }
                    ArrayAdapter<String> adapterCategory = new ArrayAdapter<String>(mainView.getContext(), android.R.layout.simple_spinner_item, cats);
                    adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categoryFilter.setAdapter(adapterCategory);
                    categoryFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position != 0)
                                categoryId = categories[position - 1].getCategoryID();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            } catch (DBException e) {
                e.printStackTrace();
            }

            saveFilters.setOnClickListener(this);
            reset.setOnClickListener(this);
        }
        dialog.show();
    }

    private void goToProductFragment(String productID) {
        Bundle arg = new Bundle();
        arg.putString(ProductFragment.PRODUCT_KEY, productID);
        ((MainActivity) getActivity()).replaceFragments(ProductFragment.class, arg, MainActivity.PRODUCT_TAG, true);
    }

    @Override
    public void onClick(View v) {
        int value = 0;
        switch (v.getId()) {
            case R.id.rating_star5:
                value++;
            case R.id.rating_star4:
                value++;
            case R.id.rating_star3:
                value++;
            case R.id.rating_star2:
                value++;
            case R.id.rating_star1:
                value++;
                turnOnStars(filterStars, value);
                ratingFilter = value;
                break;
            case R.id.save_btn:
                if (ratingFilter != 0)
                    greater.put(BackendInterface.RATING, (double) ratingFilter);
                if (!isEmpty(fromFilter)) {
                    from = Integer.parseInt(fromFilter.getText().toString());
                    greater.put(BackendInterface.PRICE, (double) from);
                }
                if (!isEmpty(toFilter)) {
                    to = Integer.parseInt(toFilter.getText().toString());
                    less.put(BackendInterface.PRICE, (double) to);
                }
                doQuery();
                reset.setVisibility(View.VISIBLE);
                dialog.hide();
                break;
            case R.id.reset:
                ratingFilter = 0;
                turnOnStars(filterStars, 0);

                from = 0;
                fromFilter.setText("");
                greater.clear();

                to = 0;
                toFilter.setText("");
                less.clear();

                categoryId = null;
                categoryFilter.setSelection(0);

                doQuery();
                reset.setVisibility(View.GONE);
                dialog.hide();
                break;
        }
    }

    private void turnOnStars(ImageView[] stars, int value) {
        for (int i = 0; i < stars.length; i++) {
            if (i < value)
                stars[i].setImageResource(R.drawable.ic_full_star_24dp);
            else
                stars[i].setImageResource(R.drawable.ic_empty_star_24dp);
        }
    }

    class Adapter extends ArrayAdapter<Product> {
        Product[] products;
        Context context;
        int layoutID;

        Adapter(@NonNull Context context, int layoutID, @NonNull Product[] products) {
            super(context, layoutID, products);
            this.context = context;
            this.products = products;
            this.layoutID = layoutID;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService((Context.LAYOUT_INFLATER_SERVICE));
            convertView = layoutInflater.inflate(layoutID, null);

            ImageView image = convertView.findViewById(R.id.product_image);
            TextView title = convertView.findViewById(R.id.title);
            ImageView[] stars = {convertView.findViewById(R.id.star1), convertView.findViewById(R.id.star2), convertView.findViewById(R.id.star3), convertView.findViewById(R.id.star4), convertView.findViewById(R.id.star5)};
            TextView price = convertView.findViewById(R.id.price);
            TextView numberRews = convertView.findViewById(R.id.number_rews);

            title.setText(products[position].getTitle());
            price.setText(Float.toString(products[position].getPrice()) + "€");
            String[] urls = products[position].getImageURLs();
            if (urls != null && urls.length > 0)
                Picasso.get().load(urls[0]).into(image);

            float fRating = products[position].getAvgRating();
            int iRating = (int) fRating;

            for (int i = 0; i < iRating; i++) {
                stars[i].setImageResource(R.drawable.ic_full_star_24dp);
            }

            //perché int tronca mentre math arrotonda per eccesso sopra il x.5
            if (iRating != Math.round(fRating)) {
                stars[iRating].setImageResource(R.drawable.ic_half_star_24dp);
            }

            int nrw = 0;
            if (products[position].getReviews() != null)
                nrw = products[position].getReviews().length;
            numberRews.setText("(" + nrw + ")");

            return convertView;
        }
    }

    private boolean isEmpty(EditText e) {
        return e.getText().toString().trim().length() == 0;
    }
}
