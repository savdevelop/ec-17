package com.gr42.ec_17.control.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etPassword;
    private TextView tvError;
    private Button btnSignUp;
    private UserController userController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        etName = findViewById(R.id.etName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        tvError = findViewById(R.id.tvError);
        btnSignUp = findViewById(R.id.btnSignUp);
        userController = UserController.getInstance();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:
                EditText[] etComponents = {etName, etLastName, etEmail, etPassword};
                boolean noEmptyFields = true;
                for (EditText et : etComponents) {
                    if (et.getText().length() == 0) {
                        noEmptyFields = false;
                        YoYo.with(Techniques.Shake)
                                .duration(700)
                                .playOn(et);
                        tvError.setText(this.getString(R.string.formError) + " " + et.getHint().toString().toLowerCase());
                        break;
                    }
                }
                if (noEmptyFields) {
                    String e = userController.signUp(SignUpActivity.this, etName.getText().toString(), etLastName.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString());
                    if (e == null) {
                        Toast.makeText(SignUpActivity.this, "Thanks for registering. Welcome!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        tvError.setText(this.getString(R.string.serverError) + " " + e);
                    }
                }
                break;
            case R.id.tvLogin:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }
}
