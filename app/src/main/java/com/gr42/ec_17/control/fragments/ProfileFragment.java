package com.gr42.ec_17.control.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.BackendInterface;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.entity.User;

import java.util.Calendar;

public class ProfileFragment extends Fragment {

    private User user;
    private UserController userController;
    private EditText etName;
    private EditText etLastName;
    private TextView tvDate;
    private RadioGroup rgGender;
    private int[] rbArray = {R.id.rbMale, R.id.rbFemale};
    private EditText etPhone;
    private EditText etEmail;
    private EditText etPassword;
    private TextView tvError;
    private Button btnSave;
    private Calendar date;
    private View mainView;

    private DatePickerDialog.OnDateSetListener tvDateListner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_profile, container, false);
        userController = UserController.getInstance();
        initComponents();

        try {
            user = userController.getUserInformation();
            etName.setText(user.getName());
            etLastName.setText(user.getLastName());
            etEmail.setText(user.getEmail());
            etPassword.setText(user.getPassword());
            date = user.getDateOfBirth();
            if(date != null) {
                tvDate.setText(date.get(Calendar.DAY_OF_MONTH) + "/" + (date.get(Calendar.MONTH) + 1) + "/" + date.get(Calendar.YEAR));
                tvDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i("DATE", "CLICKED");
                        Calendar calendar = Calendar.getInstance();
                        DatePickerDialog dialog = new DatePickerDialog(v.getContext(), android.R.style.Theme_DeviceDefault_Dialog, tvDateListner, date.get(Calendar.YEAR), (date.get(Calendar.MONTH) + 1), date.get(Calendar.DAY_OF_MONTH));
                        dialog.show();
                    }
                });
                tvDateListner = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        date.set(year, month, dayOfMonth);
                        tvDate.setText(dayOfMonth + "/" + month + 1 + "/" + year);
                    }
                };
            }

            rgGender.check(rbArray[user.getSex()]);
            if(user.getPhoneNumber()!=null)
                etPhone.setText(user.getPhoneNumber());

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    user.setName(etName.getText().toString());
                    user.setLastName(etLastName.getText().toString());
                    user.setDateOfBirth(date);
                    int sex=0;
                    switch (rgGender.getCheckedRadioButtonId()) {
                        case R.id.rbMale:
                            sex = 0;
                            break;
                        case R.id.rbFemale:
                            sex = 1;
                            break;
                    }
                    user.setPhoneNumber(etPhone.getText().toString());
                    user.setEmail(etEmail.getText().toString());
                    user.setPassword(etPassword.getText().toString());
                    user.setSex(sex);
                    try {
                        userController.updateUserInformation(user);
                        Toast.makeText(mainView.getContext(), "Done!", Toast.LENGTH_SHORT).show();
                    } catch (DBException e) {
                        if (e.getMessage().toString().equals(BackendInterface.EMAIL_ERROR)) {
                            ManageError(etEmail, R.string.email);
                        } else if (e.getMessage().equals(BackendInterface.PASSWORD_ERROR)) {
                            ManageError(etPassword, R.string.password);
                        } else if (e.getMessage().equals(BackendInterface.SERVER_ERROR)) {
                            ManageError(null, R.string.server);
                        }
                    }

                }
            });
        } catch (DBException e) {
            e.printStackTrace();
        }


        return mainView;
    }

    private void initComponents() {
        etName = mainView.findViewById(R.id.etName);
        etLastName = mainView.findViewById(R.id.etLastName);
        tvDate = mainView.findViewById(R.id.tvDate);
        rgGender = mainView.findViewById(R.id.rgGender);
        etPhone = mainView.findViewById(R.id.etPhone);
        etEmail = mainView.findViewById(R.id.etEmail);
        etPassword = mainView.findViewById(R.id.etPassword);
        tvError = mainView.findViewById(R.id.tvError);
        btnSave = mainView.findViewById(R.id.btnSave);
    }

    private void ManageError(View v, int whoID) {
        if (v != null) {
            YoYo.with(Techniques.Shake)
                    .duration(700)
                    .playOn(v);
        }
        tvError.setText(getString(whoID) + getString(R.string.error));

    }

}
