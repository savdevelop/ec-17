package com.gr42.ec_17.control.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.Address;
import com.gr42.ec_17.entity.Country;

public class NewEditFragment extends Fragment {
    public static final String ID_KEY = "ADDR_ID";
    private View mainView;
    private Address address;
    private Country[] countries;
    private UserController userController;
    private EditText title, name, lastname, city, street;
    private Button btnSave;
    private Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_new_edit_address, container, false);
        initComponents();
        userController = UserController.getInstance();

        address = new Address();

        try {
            countries = userController.getCountries();
            //spinner setup
            String[] values = new String[countries.length];
            for (int i = 0; i < countries.length; i++) {
                values[i] = countries[i].getName();
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainView.getContext(), android.R.layout.simple_spinner_item, values);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }


        Bundle extras = getArguments();
        if (extras != null) {
            if (extras.containsKey(ID_KEY)) {
                try {
                    TextView pageTitle = mainView.findViewById(R.id.page_title);
                    pageTitle.setText(getString(R.string.edit_address));
                    address = userController.getAddress(extras.getString(ID_KEY));
                    title.setText(address.getTitle());
                    name.setText(address.getName());
                    lastname.setText(address.getLastName());
                    for (int i = 0; i < countries.length; i++) {
                        if(countries[i].getCountryID().equals(address.getCountry().getCountryID())){
                            spinner.setSelection(i);
                            break;
                        }
                    }
                    city.setText(address.getCity());
                    street.setText(address.getStreet());
                } catch (DBException e) {
                    Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText[] etComponents = {title, name, lastname, city, street};
                boolean noEmptyFields = true;
                for (EditText et : etComponents) {
                    if (et.getText().length() == 0) {
                        noEmptyFields = false;
                        YoYo.with(Techniques.Shake)
                                .duration(700)
                                .playOn(et);
                        break;
                    }
                }
                if (noEmptyFields) {
                    address.setTitle(title.getText().toString());
                    address.setName(name.getText().toString());
                    address.setLastName(lastname.getText().toString());
                    address.setCountry(countries[spinner.getSelectedItemPosition()]);
                    address.setCity(city.getText().toString());
                    address.setStreet(street.getText().toString());
                    try {
                        int res;
                        if (address.getAddressID() == null) {
                            userController.addAddress(address);
                            res = R.string.address_added;
                        } else {
                            userController.editAddress(address);
                            res = R.string.address_edited;
                        }
                        Toast.makeText(mainView.getContext(), res, Toast.LENGTH_SHORT).show();
                        goToAddress();
                    } catch (DBException e) {
                        Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return mainView;
    }

    private void initComponents() {
        btnSave = mainView.findViewById(R.id.save_changes);
        title = mainView.findViewById(R.id.address_title);
        name = mainView.findViewById(R.id.name);
        lastname = mainView.findViewById(R.id.lastname);
        city = mainView.findViewById(R.id.city);
        street = mainView.findViewById(R.id.street);
        spinner = mainView.findViewById(R.id.country);
    }

    private void goToAddress() {
        ((MainActivity) getActivity()).replaceFragments(AddressesFragment.class, null, MainActivity.ADDRESSES_TAG, true);
    }
}
