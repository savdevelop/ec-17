package com.gr42.ec_17.control.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.Address;

public class AddressesFragment extends Fragment {
    private Address[] addresses = null;
    private View mainView;
    private UserController userController;
    private LinearLayout addressesLayout;
    private ImageView add;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_addresses, container, false);
        userController = UserController.getInstance();
        addressesLayout = mainView.findViewById(R.id.addresses);
        add = mainView.findViewById(R.id.add);

        try {
            addresses = userController.getAddresses();
            if (addresses != null) {
                for (int i = 0; i < addresses.length; i++) {
                    addRow(addressesLayout, addresses[i]);
                    if (i != addresses.length - 1)
                        addDivider(addressesLayout);
                }

            }
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToNewEditAddress(null);
                }
            });
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return mainView;
    }

    private void addRow(ViewGroup mLinearLayout, final Address address) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.row_address, mLinearLayout, false);

        TextView title, name, lastname, country, city, street;
        final ImageView edit, remove;
        title = row.findViewById(R.id.address_title);
        name = row.findViewById(R.id.name);
        lastname = row.findViewById(R.id.lastname);
        country = row.findViewById(R.id.country);
        city = row.findViewById(R.id.city);
        street = row.findViewById(R.id.street);
        edit = row.findViewById(R.id.edit_address);
        remove = row.findViewById(R.id.remove_address);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNewEditAddress(address.getAddressID());
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    userController.removeAddress(address.getAddressID());
                    ((MainActivity) getActivity()).refreshCurrentFragment(MainActivity.ADDRESSES_TAG);
                } catch (DBException e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        title.setText(address.getTitle());
        name.setText(address.getName());
        lastname.setText(address.getLastName());
        if(address.getCountry() != null)
            country.setText(address.getCountry().getName());
        city.setText(address.getCity());
        street.setText(address.getStreet());

        mLinearLayout.addView(row);
    }

    private void addDivider(ViewGroup mLinearLayout) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.divider, mLinearLayout, false);
        mLinearLayout.addView(row);
    }

    private void goToNewEditAddress(String addressID) {
        Bundle arg = null;
        if (addressID != null) {
            arg = new Bundle();
            arg.putString(NewEditFragment.ID_KEY, addressID);
        }
        ((MainActivity) getActivity()).replaceFragments(NewEditFragment.class, arg, MainActivity.NEWEDITADRR_TAG, false);
    }
}
