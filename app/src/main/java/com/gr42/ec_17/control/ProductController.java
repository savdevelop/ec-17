package com.gr42.ec_17.control;

import android.content.Context;
import android.content.SharedPreferences;

import com.gr42.ec_17.App;
import com.gr42.ec_17.DBException;
import com.gr42.ec_17.entity.Category;
import com.gr42.ec_17.entity.Product;

import java.util.ArrayList;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.gr42.ec_17.control.UserController.USERID_KEY;

public class ProductController {

    private BackendInterface backendInterface;
    private String userID = null;

    //Singleton setup
    private static final ProductController INSTANCE = new ProductController();

    private ProductController() {
        backendInterface = BackendInterface.getInstance();
        Context context = App.getContext();
        SharedPreferences sp = context.getSharedPreferences(UserController.LOGIN_FILENAME, MODE_PRIVATE);
        userID = sp.getString(USERID_KEY, null);
    }

    public static ProductController getInstance() {
        return INSTANCE;
    }

    public Product[] getProducts(String name, String categoryID, Map<String, Double> greaterThan, Map<String, Double> lessThan, Map<String, Boolean> orderBy, int limit) throws DBException {
        return backendInterface.getProducts(name, categoryID, greaterThan, lessThan, orderBy, limit);
    }

    public Product getProduct(String productID) throws DBException {
        return backendInterface.getProduct(productID);
    }

    public void addProductToCart(String productID, int quantity) throws DBException {
        backendInterface.addProductToCart(productID, quantity);
    }

    public void addReview(String title, String description, int rating) throws DBException {
        backendInterface.addReview(title, description, rating);
    }

    public Category[] getCategories() throws DBException {
        return backendInterface.getCategories();
    }

    public Product[] getTrendingProducts() throws DBException {
        return backendInterface.getTopSellerProducts();
    }

    public Product[] getNewestProducts() throws DBException {
        return backendInterface.getNewestProducts();
    }

    public Product[] getTopRatedProducts() throws DBException {
        return backendInterface.getTopRatedProducts();
    }

}
