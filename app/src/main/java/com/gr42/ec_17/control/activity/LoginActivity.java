package com.gr42.ec_17.control.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail;
    private EditText etPassword;
    private Button btnLogin;
    private TextView tvError;
    private UserController userController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvError = findViewById(R.id.tvError);
        userController = UserController.getInstance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                EditText[] etComponents = {etEmail, etPassword};
                boolean noEmptyFields = true;
                for (EditText et : etComponents) {
                    if (et.getText().length() == 0) {
                        noEmptyFields = false;
                        YoYo.with(Techniques.Shake)
                                .duration(700)
                                .playOn(et);
                        tvError.setText(this.getString(R.string.formError) + " "+ et.getHint().toString().toLowerCase());
                        break;
                    }
                }
                if (noEmptyFields) {
                    String e = userController.login(LoginActivity.this, etEmail.getText().toString(), etPassword.getText().toString());
                    if(e == null){
                        Toast.makeText(LoginActivity.this, "Welcome back!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        switch (e){
                            case "email":
                                YoYo.with(Techniques.Shake)
                                        .duration(700)
                                        .playOn(etEmail);
                                tvError.setText(this.getString(R.string.wrong) + " "+ etEmail.getHint().toString().toLowerCase());
                                break;
                            case "password":
                                YoYo.with(Techniques.Shake)
                                        .duration(700)
                                        .playOn(etPassword);
                                tvError.setText(this.getString(R.string.wrong) + " "+ etPassword.getHint().toString().toLowerCase());
                                break;
                            default:
                                tvError.setText(this.getString(R.string.serverError));
                        }
                    }
                }
                break;
            case R.id.etSignUp:
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }
}
