package com.gr42.ec_17.control.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.BackendInterface;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.BoughtProduct;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class CartFragment extends Fragment {
    private View mainView;
    private LinearLayout productsLayout;
    private TextView tvSubtotal;
    private Button checkoutBtn;
    private UserController userController;
    private BoughtProduct[] products = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_cart, container, false);
        initComponents();
        userController = UserController.getInstance();

        try {
            float subtotal = 0;
            products = userController.getCart();
            for (int i = 0; i < products.length; i++) {
                addRow(productsLayout, products[i]);
                subtotal += products[i].getPrice() * products[i].getBoughtQuantity();
                if (i != products.length - 1)
                    addDivider(productsLayout);
            }
            tvSubtotal.setText(getString(R.string.currency) + Float.toString(subtotal));
            checkoutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (products != null && products.length > 0)
                        goToCheckout();
                }
            });
        } catch (DBException e) {
            String error = e.getMessage();
            if (e.getMessage().equals(BackendInterface.CART_ERROR))
                error = getString(R.string.empty_cart);
            Toast.makeText(mainView.getContext(), error, Toast.LENGTH_SHORT).show();
        }

        return mainView;
    }

    private void initComponents() {
        productsLayout = mainView.findViewById(R.id.productsLayout);
        tvSubtotal = mainView.findViewById(R.id.subtotal);
        checkoutBtn = mainView.findViewById(R.id.checkoutBtn);
    }

    private void addRow(ViewGroup mLinearLayout, final BoughtProduct product) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.row_cart_product, mLinearLayout, false);

        TextView title, price;
        Spinner quantity;
        final ImageView remove, image;
        title = row.findViewById(R.id.title);
        price = row.findViewById(R.id.price);
        quantity = row.findViewById(R.id.quantity_spinner);
        remove = row.findViewById(R.id.remove_address);
        image = row.findViewById(R.id.imageView);

        title.setText(product.getTitle());

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductFragment(product.getProductID());
            }
        });

        price.setText(getString(R.string.currency) + Float.toString(product.getPrice()));
        String[] urls = product.getImageURLs();
        if (urls != null && urls.length > 0)
            Picasso.get().load(urls[0]).into(image);

        String[] values = new String[product.getQuantityAvailable()];
        for (int j = 0; j < product.getQuantityAvailable(); j++) {
            values[j] = Integer.toString(j + 1);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainView.getContext(), android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quantity.setAdapter(adapter);
        quantity.setSelection(product.getBoughtQuantity() - 1);

        quantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    int newQuant = Integer.parseInt(parent.getSelectedItem().toString());
                    if (product.getBoughtQuantity() != newQuant) {
                        product.setBoughtQuantity(Integer.parseInt(parent.getSelectedItem().toString()));
                        userController.changeQuantityProductCart(product.getProductID(), product.getBoughtQuantity());
                    }
                } catch (DBException e) {
                    Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                updateSubtotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    userController.removeProductFromCart(product.getProductID());
                    ((MainActivity) getActivity()).refreshCurrentFragment(MainActivity.CART_TAG);
                } catch (DBException e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mLinearLayout.addView(row);
    }

    private void addDivider(ViewGroup mLinearLayout) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.divider, mLinearLayout, false);
        mLinearLayout.addView(row);
    }

    private void goToCheckout() {
        ((MainActivity) getActivity()).replaceFragments(OrderSummaryFragment.class, null, MainActivity.ORDERSUM_TAG, true);
    }

    private void updateSubtotal() {
        float subtotal = 0;
        for (BoughtProduct p : products) {
            subtotal += p.getPrice() * p.getBoughtQuantity();
        }
        tvSubtotal.setText(getString(R.string.currency) + Float.toString(subtotal));
    }

    private void goToProductFragment(String productID) {
        Bundle arg = new Bundle();
        arg.putString(ProductFragment.PRODUCT_KEY, productID);
        ((MainActivity) getActivity()).replaceFragments(ProductFragment.class, arg, MainActivity.PRODUCT_TAG, true);
    }
}
