package com.gr42.ec_17.control.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.entity.BoughtProduct;
import com.gr42.ec_17.entity.Order;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

public class OrdersFragment extends Fragment {
    private Order[] orders = null;
    private ListView listOrders;
    private UserController userController;
    private View mainView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_orders, container, false);
        listOrders = mainView.findViewById(R.id.orders);

        userController = UserController.getInstance();

        try {
            orders = userController.getOrders();
            if (orders != null) {
                Adapter adapter = new Adapter(mainView.getContext(), R.layout.row_order, orders);
                listOrders.setAdapter(adapter);
            }
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return mainView;
    }

    class Adapter extends ArrayAdapter<Order> {
        Order[] orders;
        int layoutID;
        Context context;

        public Adapter(@NonNull Context context, int layoutID, @NonNull Order[] orders) {
            super(context, layoutID, orders);
            this.orders = orders;
            this.layoutID = layoutID;
            this.context = context;
        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService((Context.LAYOUT_INFLATER_SERVICE));
            convertView = layoutInflater.inflate(layoutID, null);

            TextView orderNumber, address, tvDate;
            LinearLayout layoutProducts;

            orderNumber = convertView.findViewById(R.id.order_number);
            address = convertView.findViewById(R.id.address);
            tvDate = convertView.findViewById(R.id.date);
            layoutProducts = convertView.findViewById(R.id.bought_product_layout);

            orderNumber.setText(getString(R.string.order_number) + " " + orders[position].getOrderID());
            address.setText(orders[position].getAddress());
            Calendar date = orders[position].getCheckoutDate();
            tvDate.setText(date.get(Calendar.DAY_OF_MONTH) + "/" + (date.get(Calendar.MONTH) + 1) + "/" + date.get(Calendar.YEAR));


            BoughtProduct[] products = orders[position].getProducts();

            for (BoughtProduct p : products) {
                addRow(layoutProducts, p);
            }


            return convertView;
        }

        private void addRow(ViewGroup mLinearLayout, BoughtProduct product) {
            View row = LayoutInflater.from(context).inflate(R.layout.row_bought_product, mLinearLayout, false);

            TextView title, price, quantity;
            ImageView image;
            title = row.findViewById(R.id.title);
            price = row.findViewById(R.id.price);
            quantity = row.findViewById(R.id.quantity);
            image = row.findViewById(R.id.imageView);

            title.setText(product.getTitle());
            price.setText(getString(R.string.currency) + Float.toString(product.getPrice()));
            quantity.setText(getString(R.string.quantity) + product.getBoughtQuantity());
            String[] urls = product.getImageURLs();
            if (urls != null && urls.length > 0)
                Picasso.get().load(urls[0]).into(image);

            mLinearLayout.addView(row);
        }

        private void addDivider(ViewGroup mLinearLayout) {
            View row = LayoutInflater.from(context).inflate(R.layout.divider, mLinearLayout, false);
            mLinearLayout.addView(row);
        }
    }

}
