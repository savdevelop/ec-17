package com.gr42.ec_17.control.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.Address;
import com.gr42.ec_17.entity.BoughtProduct;
import com.gr42.ec_17.entity.Order;
import com.squareup.picasso.Picasso;

public class OrderSummaryFragment extends Fragment {
    private View mainView;
    private LinearLayout productsLayout;
    private TextView tvSubtotal, prod_subtot, ship_cost;
    private Spinner addressSpinner;
    private Button checkoutBtn;
    private UserController userController;
    private BoughtProduct[] products;
    private Address[] addresses;
    private int selectedAddress;
    private float itemsCost = 0;
    private float shippingCost = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_order_summary, container, false);
        initComponents();
        userController = UserController.getInstance();

        try {
            float subtotal = 0;
            products = userController.getCart();
            addresses = userController.getAddresses();

            String[] values = null;
            if (addresses != null) {
                values = new String[addresses.length];
                for (int i = 0; i < addresses.length; i++) {
                    values[i] = addresses[i].getTitle();
                }
            } else {
                values = new String[1];
                values[0] = "Create an address first.";
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainView.getContext(), android.R.layout.simple_spinner_item, values);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            addressSpinner.setAdapter(adapter);
            addressSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(addresses != null)
                        shippingCost = addresses[position].getCountry().getShippingCost();
                    else
                        shippingCost = 0;
                    selectedAddress = position;
                    updateCosts();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            for (int i = 0; i < products.length; i++) {
                addRow(productsLayout, products[i]);
                subtotal += products[i].getPrice() * products[i].getBoughtQuantity();
                if (i != products.length - 1)
                    addDivider(productsLayout);
            }
            tvSubtotal.setText(getString(R.string.currency) + Float.toString(subtotal));

            checkoutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Order order = new Order(null, products, null, null);
//                    try {
//                        userController.addOrder(addresses[selectedAddress]);
//                        Toast.makeText(mainView.getContext(), "Congrats!", Toast.LENGTH_SHORT).show();
//                    } catch (DBException e) {
//                        Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
                    if (addresses != null) {
                        goToPayment(addresses[selectedAddress]);
                    } else {
                        Toast.makeText(mainView.getContext(), "Please create an address first", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            updateCosts();
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return mainView;
    }

    private void initComponents() {
        productsLayout = mainView.findViewById(R.id.productsLayout);
        tvSubtotal = mainView.findViewById(R.id.subtotal);
        checkoutBtn = mainView.findViewById(R.id.paymentBtn);
        addressSpinner = mainView.findViewById(R.id.address);
        prod_subtot = mainView.findViewById(R.id.prod_subtot);
        ship_cost = mainView.findViewById(R.id.ship_cost);
    }

    private void addRow(ViewGroup mLinearLayout, final BoughtProduct product) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.row_order_summary, mLinearLayout, false);

        TextView title, price, quantity;
        ImageView image;
        title = row.findViewById(R.id.title);
        price = row.findViewById(R.id.price);
        quantity = row.findViewById(R.id.prod_quantity);
        image = row.findViewById(R.id.imageView);


        title.setText(product.getTitle());
        price.setText(getString(R.string.currency) + Float.toString(product.getPrice()));
        quantity.setText(Integer.toString(product.getBoughtQuantity()));
        String[] urls = product.getImageURLs();
        if (urls != null && urls.length > 0)
            Picasso.get().load(urls[0]).into(image);

        itemsCost += product.getPrice() * product.getBoughtQuantity();

        mLinearLayout.addView(row);
    }

    private void addDivider(ViewGroup mLinearLayout) {
        View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.divider, mLinearLayout, false);
        mLinearLayout.addView(row);
    }

    private void updateCosts() {
        float total = getTotal();
        ship_cost.setText(getString(R.string.currency) + shippingCost);
        prod_subtot.setText(getString(R.string.currency) + itemsCost);
        tvSubtotal.setText(getString(R.string.currency) + total);
    }

    private float getTotal() {
        return (float) itemsCost + shippingCost;
    }

    private void goToPayment(Address address) {
        ((MainActivity) getActivity()).goToPaypal(getTotal(), address);
    }

}




