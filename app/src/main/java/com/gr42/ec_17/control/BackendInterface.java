package com.gr42.ec_17.control;

import android.util.Log;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.entity.Address;
import com.gr42.ec_17.entity.BoughtProduct;
import com.gr42.ec_17.entity.Category;
import com.gr42.ec_17.entity.Country;
import com.gr42.ec_17.entity.Order;
import com.gr42.ec_17.entity.Product;
import com.gr42.ec_17.entity.Review;
import com.gr42.ec_17.entity.User;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class BackendInterface {
    //Throws
    public static final String EMAIL_ERROR = "email";
    public static final String PASSWORD_ERROR = "password";
    public static final String SERVER_ERROR = "server";
    public static final String PRODUCT_ERROR = "product";
    public static final String REVIEW_ERROR = "review";
    public static final String CART_ERROR = "cart";

    //Tables
    private static final String OBJECT_ID = "objectId";
    private static final String PROFILE = "ProfileInformation";
    private static final String USER = "UserCredential";
    private static final String PRODUCT = "Product";
    private static final String REVIEW = "Review";
    private static final String PRODUCT_IMAGE = "ProductImage";
    private static final String SEARCHES = "Searches";
    private static final String CART = "Cart";
    private static final String ORDER = "Order";
    private static final String ADDRESS = "Address";
    private static final String COUNTRY = "Country";
    private static final String CATEGORY = "Category";

    //Filters and order fields
    public static final String RATING = "avgRating";
    public static final String PRICE = "price";
    public static final String REVIEWS = "timesReviewed";
    public static final String ORDERS = "timesBought";
    public static final String TIME = "createdAt";

    public static final boolean ASCENDING = true;
    public static final boolean DESCENDING = false;


    //Singleton setup
    private static final BackendInterface INSTANCE = new BackendInterface();
    private ParseObject userObject;
    private ParseObject currentProduct;
    private String userID1;

    public BackendInterface() {
    }

    public static BackendInterface getInstance() {
        return INSTANCE;
    }

    public void retrieveUser(String userID) throws DBException {
        ParseQuery<ParseObject> query = new ParseQuery<>(USER);
        query.whereEqualTo(OBJECT_ID, userID);
        try {
            List<ParseObject> obj = query.find();
            if (obj.size() > 0) {
                userObject = obj.get(0);
                userID1 = userObject.getObjectId();
            }
        } catch (ParseException e) {
            throw new DBException(SERVER_ERROR);
        }
    }

    public String signUp(final String name, final String lastName, String email, String password) {
        String error = null;

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("UserCredential");
        query.whereEqualTo("email", email);
        try {
            List<ParseObject> objs = query.find();
            if (objs.size() == 0) {
                userObject = new ParseObject("UserCredential");
                userObject.put("email", email);
                userObject.put("password", password);
                try {
                    userObject.save();
                    Log.i("SIGNUP", "USER CREATED ");
                    ParseObject profileInformation = new ParseObject("ProfileInformation");
                    profileInformation.put("name", name);
                    profileInformation.put("lastName", lastName);
                    profileInformation.put("userFK", userObject);
                    try {
                        profileInformation.save();
                        error = userObject.getObjectId();
                        userID1 = userObject.getObjectId();
                        Log.i("SIGNUP", "PROFILE INFO ADDED");
                    } catch (ParseException e) {
                        error = SERVER_ERROR;
                        Log.e("SIGNUP", e.getMessage().toString());
                    }
                } catch (ParseException e) {
                    error = SERVER_ERROR;
                    Log.e("SIGNUP", e.getMessage().toString());
                }
            } else {
                error = EMAIL_ERROR;
            }
        } catch (ParseException e) {
            error = SERVER_ERROR;
            Log.e("SIGNUP", e.getMessage().toString());
        }
        return error;
    }

    public String login(String email, String password) {
        String error = null;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserCredential");
        query.whereEqualTo("email", email);
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                Log.i("LOGIN", "EMAIL OK");
                userObject = objects.get(0);
                if (userObject.get("password").toString().equals(password)) {
                    Log.i("LOGIN", "PASSWORD OK");
                    error = userObject.getObjectId();
                    userID1 = userObject.getObjectId();
                } else {
                    Log.i("LOGIN", "WRONG PASSWORD");
                    error = PASSWORD_ERROR;
                }
            } else {
                error = EMAIL_ERROR;
                Log.i("LOGIN", "WRONG EMAIL ");
            }
        } catch (ParseException e) {
            error = e.getMessage().toString();
            Log.i("LOGIN", "SERVER ERROR");
        }

        return error;
    }

    public User getUserInformation(String userID) throws DBException {
        User user = null;
        ParseQuery<ParseObject> innerQuery = new ParseQuery<>(USER);
        innerQuery.whereEqualTo(OBJECT_ID, userID1);
        ParseQuery<ParseObject> query = new ParseQuery<>(PROFILE);
        query.whereMatchesQuery("userFK", innerQuery);
        query.include("userFK");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject o = objects.get(0);
                ParseObject cred = o.getParseObject("userFK");
                user = new User(o.get("name").toString(), o.get("lastName").toString(), cred.get("email").toString(), cred.get("password").toString());
                Date d = null;
                d = (Date) o.get("dateOfBirth");
                if (d != null) {
                    Calendar date = Calendar.getInstance();
                    date.setTime(d);
                    user.setDateOfBirth(date);
                }

                String phone = null;
                Object ob = o.get("phoneNumber");
                if (ob != null)
                    phone = ob.toString();
                user.setPhoneNumber(phone);
                ob = o.get("sex");
                if (ob != null)
                    user.setSex(Integer.parseInt(ob.toString()));
            }
        } catch (ParseException e) {
            throw new DBException(SERVER_ERROR);
        }

        return user;
    }

    public void updateUserInformation(User user, String userID) throws DBException {
        ParseQuery<ParseObject> innerQuery = new ParseQuery<>(USER);
        innerQuery.whereEqualTo(OBJECT_ID, userID1);
        ParseQuery<ParseObject> query = new ParseQuery<>(PROFILE);
        query.whereMatchesQuery("userFK", innerQuery);
        query.include("userFK");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject o = objects.get(0);
                ParseObject cred = o.getParseObject("userFK");
                o.put("name", user.getName());
                o.put("lastName", user.getLastName());
                o.put("sex", user.getSex());
                o.put("phoneNumber", user.getPhoneNumber());
                cred.put("email", user.getEmail());
                cred.put("password", user.getPassword());
                o.put("dateOfBirth", user.getDateOfBirth().getTime());
                o.saveInBackground();
                cred.saveInBackground();
            }
        } catch (ParseException e) {
            throw new DBException(SERVER_ERROR);
        }

    }

    public String[] getLatestSearchStrings(String userID) throws DBException {
        String[] searches = null;
        ParseQuery<ParseObject> innerQuery = new ParseQuery<>(USER);
        innerQuery.whereEqualTo(OBJECT_ID, userID1);
        ParseQuery<ParseObject> query = new ParseQuery<>(SEARCHES);
        query.whereMatchesQuery("user", innerQuery);
        query.orderByDescending("updatedAt");
        try {
            List<ParseObject> objects = query.find();
            searches = new String[objects.size()];
            int i = 0;
            for (ParseObject o : objects) {
                searches[i] = o.get("search").toString();
                i++;
            }
        } catch (ParseException e) {
            throw new DBException(SERVER_ERROR);
        }

        return searches;
    }

    public void addSearchString(String search) throws DBException {
        ParseQuery<ParseObject> query = new ParseQuery<>(SEARCHES);
        query.whereEqualTo("user", userObject);
        query.whereEqualTo("search", search);
        try {
            ParseObject searchObj = null;
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                searchObj = objects.get(0);
            } else {
                objects.size();
                searchObj = new ParseObject(SEARCHES);
                searchObj.put("user", userObject);
            }
            searchObj.put("search", search);
            searchObj.saveInBackground();
        } catch (ParseException e) {
            throw new DBException(SERVER_ERROR);
        }
    }

    public Product[] getProducts(String name, String categoryID, Map<String, Double> greaterThan, Map<String, Double> lessThan, Map<String, Boolean> orderBy, int limit) throws DBException {
        Product[] products = null;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Product");
        if (categoryID != null) {
            ParseObject category = ParseObject.createWithoutData(CATEGORY, categoryID);
            query.whereEqualTo("category", category);
        }
        if (name != null) {
            query.whereMatches("name", "(?i).*"+ name + ".*");
        }

        if (limit != 0)
            query.setLimit(limit);

        if (greaterThan != null && greaterThan.size() > 0) {
            for (Map.Entry<String, Double> entry : greaterThan.entrySet())
                query.whereGreaterThanOrEqualTo(entry.getKey(), entry.getValue());
        }
        if (lessThan != null && lessThan.size() > 0) {
            for (Map.Entry<String, Double> entry : lessThan.entrySet())
                query.whereLessThanOrEqualTo(entry.getKey(), entry.getValue());
        }
        if (orderBy != null && orderBy.size() > 0) {
            for (Map.Entry<String, Boolean> entry : orderBy.entrySet()) {
                if (entry.getValue() == ASCENDING) {
                    query.orderByAscending(entry.getKey());
                } else {
                    query.orderByDescending(entry.getKey());
                }
            }
        }
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                products = new Product[objects.size()];
                int i = 0;
                String mainImageID = null;
                for (ParseObject o : objects) {
                    products[i] = parseObjectToProduct(o);
                    i++;
                }
            }
        } catch (ParseException e) {
            Log.e("LOGIN", e.toString());
            throw new DBException(SERVER_ERROR);
        }

        return products;
    }

    public Product getProduct(String productID) throws DBException {
        Product product = null;

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(PRODUCT);
        query.include("reviews");
        query.include("images");
        query.include("category");
        query.whereEqualTo(OBJECT_ID, productID);
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject o = objects.get(0);
                currentProduct = objects.get(0);
                product = new Product(o.getObjectId(), o.get("name").toString(), Float.parseFloat(o.get("price").toString()), Float.parseFloat(o.get("avgRating").toString()), Integer.parseInt(o.get("availablePieces").toString()));

                ParseObject categoryObj = o.getParseObject("category");
                Category category = new Category(categoryObj.getObjectId(), null, categoryObj.get("title").toString());
                product.setCategory(category);

                if (o.get("description") != null)
                    product.setDescription(o.get("description").toString());

                //image retrieving
                List<ParseObject> images = (List<ParseObject>) o.fetch().get("images");
                if (images != null && images.size() > 0) {
                    String[] imageUrls = new String[images.size()];
                    ParseFile image = null;
                    int j = 0;
                    for (ParseObject i : images) {
                        image = i.fetch().getParseFile("image");
                        imageUrls[j] = image.getUrl();
                        j++;
                    }
                    product.setImageURLs(imageUrls);
                }

                //reviews retrieving
                List<ParseObject> reviews = (List<ParseObject>) o.get("reviews");
                if (reviews != null && reviews.size() > 0) {
                    Review[] rews = new Review[reviews.size()];
                    int i = 0;
                    for (ParseObject r : reviews) {
                        rews[i] = new Review("test", r.get("title").toString(), r.get("description").toString(), Integer.parseInt(r.get("rating").toString()));
                        i++;
                    }
                    product.setReviews(rews);
                }
            }
        } catch (ParseException e) {
        }

        return product;
    }

    public void addProductToCart(String productID, int quantity) throws DBException {
        ParseQuery<ParseObject> query = new ParseQuery<>(CART);
        query.whereEqualTo("user", userObject);
        try {
            List<ParseObject> objects = query.find();

            ParseObject cart;
            if (objects.size() > 0)
                cart = objects.get(0);
            else
                cart = new ParseObject(CART);

            cart.put("user", userObject);

            ArrayList<ParseObject> products = new ArrayList<>();
            ArrayList<ParseObject> tmpProducts = (ArrayList<ParseObject>) cart.get("products");
            if (tmpProducts != null)
                products.addAll(tmpProducts);

            int i;
            for (i = 0; i < products.size(); i++) {
                if (products.get(i).getObjectId().equals(productID)) {
                    break;
                }
            }

            ArrayList<Integer> quantities = new ArrayList<>();
            ArrayList<Integer> tmpQuantities = (ArrayList<Integer>) cart.get("quantities");
            if (tmpQuantities != null) {
                quantities.addAll(tmpQuantities);
            }

            //non ha trovato nulla
            if (i == products.size()) {
                ParseObject product = ParseObject.createWithoutData(PRODUCT, productID);
                products.add(product);
                quantities.add(quantity);
            } else {
                //trovato un prodotto
                if (quantity != 0)
                    quantities.set(i, quantity);
                else {
                    quantities.remove(i);
                    products.remove(i);
                }
            }

            cart.put("products", products);
            cart.put("quantities", quantities);
            cart.save();
        } catch (ParseException e) {
            throw new DBException(SERVER_ERROR);
        }
    }

    public void addReview(String title, String description, int rating) throws DBException {
        ParseQuery<ParseObject> query = new ParseQuery<>(REVIEW);
        query.whereEqualTo("user", userObject);
        query.whereEqualTo("product", currentProduct);
        try {
            if ((query.find()).size() == 0) {
                ParseObject review = new ParseObject(REVIEW);
                review.put("user", userObject);
                review.put("rating", rating);
                review.put("title", title);
                review.put("description", description);
                review.put("product", currentProduct);
                review.save();
            } else {
                throw new DBException(REVIEW_ERROR);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }

    }

    public void addOrder(Address address) throws DBException {
        ParseObject order = new ParseObject(ORDER);
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(CART);
        query.whereEqualTo("user", userObject);
        query.include("products");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject cart = objects.get(0);
                order.put("user", userObject);

                ArrayList<ParseObject> products = (ArrayList<ParseObject>) cart.get("products");
                order.put("products", products);

                order.put("quantities", cart.get("quantities"));

                ArrayList<Float> prices = new ArrayList<>();
                for (ParseObject p : products) {
                    prices.add(Float.parseFloat(p.get("price").toString()));
                }
                order.put("prices", prices);

                order.put("address", address.toString());

                order.save();
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }
    }

    public Order[] getOrders() throws DBException {
        Order[] orders = null;

        ParseQuery<ParseObject> query = new ParseQuery<>(ORDER);
        query.whereEqualTo("user", userObject);
        query.include("products");
        query.include("prices");
        query.include("quantities");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                orders = new Order[objects.size()];
                BoughtProduct[] products = null;
                int[] quantities = null;
                float[] prices = null;
                Date date = null;
                Calendar checkoutDate = Calendar.getInstance();
                int i = 0;
                for (ParseObject o : objects) {
                    List<ParseObject> productsObj = (List<ParseObject>) o.get("products");
                    ArrayList<Integer> quantitiesObj = (ArrayList<Integer>) o.get("quantities");
                    ArrayList<Object> pricesObj = (ArrayList<Object>) o.get("prices");
                    Log.i("WOWO", o.get("prices").getClass().toString());
                    products = new BoughtProduct[productsObj.size()];

                    String mainImageID = null;
                    for (int j = 0; j < productsObj.size(); j++) {
                        Object val = pricesObj.get(j);
                        float price;
                        if (val.getClass() == Double.class)
                            price = ((Double) val).floatValue();
                        else
                            price = ((Integer) val).floatValue();
                        int quantity = quantitiesObj.get(j);
                        products[j] = new BoughtProduct(productsObj.get(j).getObjectId(), productsObj.get(j).get("name").toString(), price, (float) 0.0, 0, quantity);

                        //retrieve delle immagini
                        if (productsObj.get(j).get("images") != null)
                            mainImageID = ((List<ParseObject>) productsObj.get(j).get("images")).get(0).getObjectId();
                        ParseQuery<ParseObject> query1 = new ParseQuery<>(PRODUCT_IMAGE);
                        query1.whereEqualTo("objectId", mainImageID);
                        List<ParseObject> res = query1.find();
                        if (res.size() > 0) {
                            ParseFile image = res.get(0).getParseFile("image");
                            String[] urls = new String[1];
                            urls[0] = image.getUrl();
                            products[j].setImageURLs(urls);
                        }
                    }


                    date = o.getCreatedAt();
                    checkoutDate.setTime(date);
                    orders[i] = new Order(o.getObjectId(), products, o.get("address").toString(), checkoutDate);
                    i++;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }

        return orders;
    }

    public void addAddress(Address address) throws DBException {
        try {
            ParseObject addressObj;
            if (address.getAddressID() != null) {
                addressObj = ParseObject.createWithoutData(ADDRESS, address.getAddressID());
            } else {
                addressObj = new ParseObject(ADDRESS);
            }
            addressObj.put("user", userObject);
            addressObj.put("country", ParseObject.createWithoutData(COUNTRY, address.getCountry().getCountryID()));
            addressObj.put("title", address.getTitle());
            addressObj.put("name", address.getName());
            addressObj.put("lastName", address.getLastName());
            addressObj.put("city", address.getCity());
            addressObj.put("street", address.getStreet());

            addressObj.save();
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }

    }

    public Address[] getAddresses() throws DBException {
        Address[] addresses = null;

        ParseQuery<ParseObject> query = new ParseQuery<>(ADDRESS);
        query.whereEqualTo("user", userObject);
        query.include("country");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                addresses = new Address[objects.size()];
                Country country = null;
                ParseObject c = null;
                int i = 0;
                for (ParseObject o : objects) {
                    c = (ParseObject) o.get("country");
                    country = new Country(c.getObjectId(), c.get("name").toString(), Float.parseFloat((c.get("shippingCost")).toString()));
                    addresses[i] = new Address(o.getObjectId(), o.get("title").toString(), o.get("name").toString(), o.get("lastName").toString(), o.get("city").toString(), o.get("street").toString(), country);
                    i++;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }

        return addresses;
    }

    public Address getAddress(String addressID) throws DBException {
        Address address = null;

        ParseQuery<ParseObject> query = new ParseQuery<>(ADDRESS);
        query.whereEqualTo(OBJECT_ID, addressID);
        query.include("country");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject o = objects.get(0);
                Country country = null;
                ParseObject c = null;
                c = (ParseObject) o.get("country");
                country = new Country(c.getObjectId(), c.get("name").toString(), Float.parseFloat((c.get("shippingCost")).toString()));
                address = new Address(o.getObjectId(), o.get("title").toString(), o.get("name").toString(), o.get("lastName").toString(), o.get("city").toString(), o.get("street").toString(), country);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }

        return address;
    }

    public void removeAddress(String addressID) throws DBException {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ADDRESS);
        query.whereEqualTo(OBJECT_ID, addressID);
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject.deleteAll(objects);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }
    }

    public BoughtProduct[] getCart() throws DBException {
        BoughtProduct[] products = null;
        ParseQuery<ParseObject> query = ParseQuery.getQuery(CART);
        query.whereEqualTo("user", userObject);
        query.include("products");
        query.include("images");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                ParseObject cart = objects.get(0);
                ArrayList<ParseObject> productsPO = (ArrayList<ParseObject>) cart.get("products");
                ArrayList<ParseObject> quantitiesPO = (ArrayList<ParseObject>) cart.get("quantities");
                products = new BoughtProduct[productsPO.size()];
                int i = 0;
                ArrayList<ParseObject> images;
                String mainImageID = null;
                for (ParseObject p : productsPO) {
                    products[i] = new BoughtProduct(p.getObjectId(), p.get("name").toString(), Float.parseFloat(p.get("price").toString()), Float.parseFloat(p.get("avgRating").toString()), Integer.parseInt(p.get("availablePieces").toString()), Integer.parseInt(String.valueOf(quantitiesPO.get(i))));

                    //retrieve delle immagini
                    if (p.get("images") != null)
                        mainImageID = ((List<ParseObject>) p.get("images")).get(0).getObjectId();
                    ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(PRODUCT_IMAGE);
                    query1.whereEqualTo("objectId", mainImageID);
                    List<ParseObject> res = query1.find();
                    if (res.size() > 0) {
                        ParseFile image = res.get(0).getParseFile("image");
                        String[] urls = new String[1];
                        urls[0] = image.getUrl();
                        products[i].setImageURLs(urls);
                    }

                    i++;
                }
            } else {
                throw new DBException(CART_ERROR);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return products;
    }

    public void changeQuantityProductCart(String userID, String productID, int quantity) throws DBException {
        ParseObject prod = ParseObject.createWithoutData(PRODUCT, productID);
        currentProduct = prod;
        addProductToCart(productID, quantity);
    }

    public void removeProductFromCart(String productID) throws DBException {
        addProductToCart(productID, 0);
    }

    public Country[] getCountries() throws DBException {
        Country[] countries = null;

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(COUNTRY);
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                countries = new Country[objects.size()];
                int i = 0;
                for (ParseObject o : objects) {
                    countries[i] = new Country(o.getObjectId(), o.get("name").toString(), o.getInt("shippingCost"));
                    i++;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }


        return countries;
    }

    public Category[] getCategories() throws DBException {
        Category[] categories = null;

        ParseQuery<ParseObject> query = new ParseQuery<>(CATEGORY);
        query.include("parent");
        try {
            List<ParseObject> objects = query.find();
            if (objects.size() > 0) {
                categories = new Category[objects.size()];
                int i = 0;
                for (ParseObject o : objects) {
                    String parent = null;
                    if (o.get("parent") != null)
                        parent = ((ParseObject) o.get("parent")).getObjectId();
                    categories[i] = new Category(o.getObjectId(), parent, o.get("title").toString());
                    i++;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new DBException(SERVER_ERROR);
        }

        return categories;
    }

    public Product[] getTopSellerProducts() throws DBException {
        Product[] products = null;
        Map<String, Boolean> orderBy = new HashMap<>();
        orderBy.put(ORDERS, DESCENDING);
        return getProducts(null, null, null, null, orderBy, 5);
    }

    public Product[] getNewestProducts() throws DBException {
        Product[] products = null;
        Map<String, Boolean> orderBy = new HashMap<>();
        orderBy.put(TIME, DESCENDING);
        return getProducts(null, null, null, null, orderBy, 5);
    }

    public Product[] getTopRatedProducts() throws DBException {
        Product[] products = null;
        Map<String, Boolean> orderBy = new HashMap<>();
        orderBy.put(RATING, DESCENDING);
        return getProducts(null, null, null, null, orderBy, 5);
    }

    public void logout(){
        userID1 = null;
        userObject = null;
        currentProduct = null;
    }

    public Product parseObjectToProduct(ParseObject o){
        Product product;
        String mainImageID = null;
        product = new Product(o.getObjectId(), o.get("name").toString(), Float.parseFloat(o.get("price").toString()), Float.parseFloat(o.get("avgRating").toString()), Integer.parseInt(o.get("availablePieces").toString()));

        //retrieve delle immagini
        if (o.get("images") != null && ((List<ParseObject>)o.get("images")).size()!= 0) {
            mainImageID = ((List<ParseObject>) o.get("images")).get(0).getObjectId();
            ParseQuery<ParseObject> query1 = new ParseQuery<>(PRODUCT_IMAGE);
            query1.whereEqualTo("objectId", mainImageID);
            List<ParseObject> res = null;
            try {
                res = query1.find();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (res.size() > 0) {
                ParseFile image = res.get(0).getParseFile("image");
                String[] urls = new String[1];
                urls[0] = image.getUrl();
                product.setImageURLs(urls);
            }
        }
        //Solo per il numero delle reviews
        if (o.get("reviews") != null && ((List<ParseObject>)o.get("reviews")).size()!= 0) {
            ArrayList<ParseObject> rews = (ArrayList<ParseObject>) o.get("reviews");
            int size = 0;
            if (rews != null) size = rews.size();
            Review[] review = new Review[size];
            product.setReviews(review);
        }

        return product;
    }

}