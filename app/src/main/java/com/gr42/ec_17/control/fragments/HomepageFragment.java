package com.gr42.ec_17.control.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.SwipeAdapter;
import com.gr42.ec_17.control.ProductController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.Product;

public class HomepageFragment extends Fragment {
    private View mainView;
    private ProductController productController;
    private ViewPager pager1, pager2, pager3;
    private Product[] products = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainView = inflater.inflate(R.layout.fragment_homepage, container, false);
        productController = ProductController.getInstance();
        pager1 = mainView.findViewById(R.id.pager1);
        pager2 = mainView.findViewById(R.id.pager2);
        pager3 = mainView.findViewById(R.id.pager3);

        try {
            String[] urls = null, productImages = null;
            SwipeAdapterHomepage adapter = null;
            int i = 0;
            products = productController.getTrendingProducts();
            if (products != null) {
                urls = new String[products.length];
                for (Product p : products) {
                    productImages = p.getImageURLs();
                    urls[i] = productImages[0];
                    i++;
                }
                adapter = new SwipeAdapterHomepage(mainView.getContext(), urls, R.layout.item_image_homepage, products);
                pager1.setAdapter(adapter);
            }

            //pager2
            products = productController.getNewestProducts();
            if (products != null) {
                i = 0;
                urls = new String[products.length];
                for (Product p : products) {
                    productImages = p.getImageURLs();
                    urls[i] = productImages[0];
                    i++;
                }
                adapter = new SwipeAdapterHomepage(mainView.getContext(), urls, R.layout.item_image_homepage, products);
                pager2.setAdapter(adapter);
            }

            //pager3
            products = productController.getTopRatedProducts();
            if (products != null) {
                i = 0;
                urls = new String[products.length];
                for (Product p : products) {
                    productImages = p.getImageURLs();
                    urls[i] = productImages[0];
                    i++;
                }
                adapter = new SwipeAdapterHomepage(mainView.getContext(), urls, R.layout.item_image_homepage, products);
                pager3.setAdapter(adapter);
            }
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        ((MainActivity)getActivity()).disableProgressBar();

        return mainView;
    }

    private class SwipeAdapterHomepage extends SwipeAdapter {
        Product[] products;

        public SwipeAdapterHomepage(Context c, String[] urls, int itemLayout, Product[] products) {
            super(c, urls, itemLayout);
            this.products = products;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View item = (View) super.instantiateItem(container, position);
            TextView title = item.findViewById(R.id.title);
            title.setText(products[position].getTitle());
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToProductFragment(products[position].getProductID());
                }
            });
            return item;
        }
    }

    private void goToProductFragment(String productID) {
        Bundle arg = new Bundle();
        arg.putString(ProductFragment.PRODUCT_KEY, productID);
        ((MainActivity) getActivity()).replaceFragments(ProductFragment.class, arg, MainActivity.PRODUCT_TAG, true);
    }
}
