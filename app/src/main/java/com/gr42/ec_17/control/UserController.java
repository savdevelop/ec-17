package com.gr42.ec_17.control;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.gr42.ec_17.App;
import com.gr42.ec_17.DBException;
import com.gr42.ec_17.entity.Address;
import com.gr42.ec_17.entity.BoughtProduct;
import com.gr42.ec_17.entity.Country;
import com.gr42.ec_17.entity.Order;
import com.gr42.ec_17.entity.User;

import static android.content.Context.MODE_PRIVATE;

public class UserController {
    public static final String LOGIN_FILENAME = "LoginPrefs";
    public static final String LOGGED_KEY = "IS_LOGGED";
    public static final String USERID_KEY = "USER_ID";

    private BackendInterface backendInterface;

    private String userID = null;

    //Singleton setup
    private static final UserController INSTANCE = new UserController();

    private UserController() {
        backendInterface = BackendInterface.getInstance();
        Context context = App.getContext();
        SharedPreferences sp = context.getSharedPreferences(LOGIN_FILENAME, MODE_PRIVATE);
        boolean isLoggedin = sp.getBoolean(LOGGED_KEY, false);
        if (isLoggedin) {
            String id = sp.getString(USERID_KEY, null);
            setUserID(id);
            try {
                backendInterface.retrieveUser(userID);
            } catch (DBException e) {
                e.printStackTrace();
            }
        }
        Log.i("USERID:", "" + userID);
    }

    public static UserController getInstance() {
        return INSTANCE;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String signUp(final Context context, final String name, final String lastName, String email, String password) {
        String returnedValue;
        returnedValue = backendInterface.signUp(name, lastName, email, password);
        if (!returnedValue.equals(BackendInterface.EMAIL_ERROR) && !returnedValue.equals(BackendInterface.SERVER_ERROR)) {
            setUserLoggedIn(context, true, returnedValue);
            returnedValue = null;
        } else {
            setUserLoggedIn(context, false, null);
        }

        return returnedValue;
    }

    public String login(final Context context, String email, String password) {
        String returnedValue;
        returnedValue = backendInterface.login(email, password);
        Log.i("LOGIN", "RETURN: " + returnedValue);
        if (!returnedValue.equals(BackendInterface.EMAIL_ERROR) && !returnedValue.equals(BackendInterface.PASSWORD_ERROR) && !returnedValue.equals(BackendInterface.SERVER_ERROR)) {
            setUserLoggedIn(context, true, returnedValue);
            returnedValue = null;
        } else {
            setUserLoggedIn(context, false, null);
        }

        return returnedValue;

    }

    private void setUserLoggedIn(Context context, boolean state, String userID) {
        SharedPreferences sp = context.getSharedPreferences(LOGIN_FILENAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(LOGGED_KEY, state);
        if (state) {
            this.userID = userID;
            editor.putString(USERID_KEY, userID);
        }
        editor.apply();
    }

    public User getUserInformation() throws DBException {
        User user = backendInterface.getUserInformation(userID);
        return user;
    }

    public void updateUserInformation(User user) throws DBException {
        Log.i("TEST", user.toString());
        backendInterface.updateUserInformation(user, userID);
    }

    public String[] getLatestSearchStrings() throws DBException {
        String[] searches = null;
        searches = backendInterface.getLatestSearchStrings(userID);
        return searches;
    }

    public void addSearchString(String search) throws DBException {
        backendInterface.addSearchString(search);
    }

    public Order[] getOrders() throws DBException {
        return backendInterface.getOrders();
    }

    public Address[] getAddresses() throws DBException {
        return backendInterface.getAddresses();
    }

    public Address getAddress(String addressID) throws DBException {
        return backendInterface.getAddress(addressID);
    }

    public void addAddress(Address address) throws DBException {
        backendInterface.addAddress(address);
    }

    public void editAddress(Address address) throws DBException {
        backendInterface.addAddress(address);
    }

    public void removeAddress(String addressID) throws DBException {
        backendInterface.removeAddress(addressID);
    }

    public BoughtProduct[] getCart() throws DBException {
        return backendInterface.getCart();
    }

    public void changeQuantityProductCart(String productID, int quantity) throws DBException {
        backendInterface.changeQuantityProductCart(userID, productID, quantity);
    }

    public void removeProductFromCart(String productID) throws DBException {
        backendInterface.removeProductFromCart(productID);
    }

    public Country[] getCountries() throws DBException {
        return backendInterface.getCountries();
    }

    public void addOrder(Address address) throws DBException {
        backendInterface.addOrder(address);
    }

    public void logout() {
        userID = null;
        backendInterface.logout();
        SharedPreferences sp = App.getContext().getSharedPreferences(LOGIN_FILENAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(LOGGED_KEY, false);
        editor.putString(USERID_KEY, null);
        editor.apply();
    }
}
