package com.gr42.ec_17.control.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gr42.ec_17.DBException;
import com.gr42.ec_17.R;
import com.gr42.ec_17.control.ProductController;
import com.gr42.ec_17.control.activity.MainActivity;
import com.gr42.ec_17.entity.Category;

public class CategoriesFragment extends Fragment {
    private View mainView;
    private LinearLayout categoriesLayout;
    private ProductController productController;
    private Category[] categories;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_categories, container, false);
        productController = ProductController.getInstance();
        categoriesLayout = mainView.findViewById(R.id.categories);

        try {
            categories = productController.getCategories();
            for (Category r : categories) {
                if (r.getParentID() == null) {
                    addRow(categoriesLayout, r, true);
                    for (Category c : categories) {
                        if (r.getCategoryID() != null && c.getParentID() != null && c.getParentID().equals(r.getCategoryID()))
                            addRow(categoriesLayout, c, false);
                    }
                }
            }
        } catch (DBException e) {
            Toast.makeText(mainView.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return mainView;
    }

    private void addRow(ViewGroup mLinearLayout, final Category category, boolean isRoot) {
        View row = null;
        if (isRoot)
            row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.row_categories_root, mLinearLayout, false);
        else
            row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.row_categories_child, mLinearLayout, false);


        TextView title;
        title = row.findViewById(R.id.title);

        title.setText(category.getTitle());

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSearchResultFragment(category.getCategoryID());
            }
        });

        mLinearLayout.addView(row);
    }

    private void goToSearchResultFragment(String categoryID) {
        Bundle arg = new Bundle();
        arg.putString(SearchResultFragment.CATEGORY_KEY, categoryID);
        ((MainActivity) getActivity()).replaceFragments(SearchResultFragment.class, arg, MainActivity.SEARCH_RESULT_TAG, true);
    }
}
