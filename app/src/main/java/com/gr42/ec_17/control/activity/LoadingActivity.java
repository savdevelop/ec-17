package com.gr42.ec_17.control.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.gr42.ec_17.R;
import com.gr42.ec_17.control.UserController;

public class LoadingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_loading);

        Thread welcomeThread = new Thread() {
            boolean isLoggedIn;
            @Override
            public void run() {
                try {
                    super.run();


                    final SharedPreferences sp = getSharedPreferences(UserController.LOGIN_FILENAME, MODE_PRIVATE);
                    isLoggedIn = sp.getBoolean(UserController.LOGGED_KEY, false);

                    //System setup
                    UserController userController = UserController.getInstance();

//                    sleep(10000);  //Delay of 10 seconds
                } catch (Exception e) {

                } finally {

                    //Login constraint check
                    if(isLoggedIn){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                    finish();
                }
            }
        };
        welcomeThread.start();
    }
}
