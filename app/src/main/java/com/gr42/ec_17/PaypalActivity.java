package com.gr42.ec_17;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class PaypalActivity extends AppCompatActivity {

    public static final String PAYPAL_KEY = "AR-Czl7FaMnMxPK1miG-zasWjaYyWB3z3jKly18q7h-OLMjGwkz8zoAg4uolV7O3X7iH0EyT60L6qRjB";
    public static final String AMOUNT_TO_PAY = "amount";
    private static final String TAG = "PAYMENT";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final String CONFIG_ENVIROMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static PayPalConfiguration config;
    PayPalPayment toBuy;

    private float amountToPay = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(AMOUNT_TO_PAY)) {
                amountToPay = extras.getFloat(AMOUNT_TO_PAY);
            }
        }

//        Toast.makeText(this, "PAGA: " + amountToPay, Toast.LENGTH_SHORT).show();
        configPaypal();
        makePayment();

    }

    private void configPaypal(){
        config = new PayPalConfiguration()
                .environment(CONFIG_ENVIROMENT)
                .clientId(PAYPAL_KEY)
                .merchantName("EC-17");

    }

    private void makePayment() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        toBuy = new PayPalPayment(new BigDecimal(amountToPay), "EUR", "EC-17 Payment", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent payment = new Intent(this, PaymentActivity.class);
        payment.putExtra(PaymentActivity.EXTRA_PAYMENT, toBuy);
        payment.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startActivityForResult(payment, REQUEST_CODE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_PAYMENT){
            if(resultCode == AppCompatActivity.RESULT_OK){
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if(confirm!=null){
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject().toString(4));
                        setResult(Activity.RESULT_OK);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            } else if(resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(this, "cancelled", Toast.LENGTH_SHORT).show();
            } else if(resultCode == PaymentActivity.RESULT_EXTRAS_INVALID){
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
