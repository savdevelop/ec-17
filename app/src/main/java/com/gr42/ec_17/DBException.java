package com.gr42.ec_17;

public class DBException extends Exception {
    public DBException(String code){
        super(code);
    }
}
