package com.gr42.ec_17;

import com.parse.Parse;
import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static Application sApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        Parse.initialize(new Parse.Configuration.Builder(this)
//                .applicationId("wZuV0RvAbmAXIKAq1GeGLcJWZRHOa4PEFby8AHt4")
//                .clientKey("Kw7XWSpOdTPYJXQAwcFPbZEOdmTbkgDUUx7oVOdh")
                .applicationId("ipKQVGVMzOYHOiorO9XcjEWBk8j9vaxLIpJevbKI")
                .clientKey("SuymWfxxHUlW2wHgvYct2QhjiA0E384915bKjdo0")
                .server("https://parseapi.back4app.com/")
                .build()
        );
    }

    public static Application getApplication(){
        return sApplication;
    }

    public static Context getContext(){
        return getApplication().getApplicationContext();
    }
}