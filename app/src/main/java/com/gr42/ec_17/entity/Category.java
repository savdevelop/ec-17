package com.gr42.ec_17.entity;

public class Category {
    private String categoryID;
    private String parentID;
    private String title;

    public Category(String categoryID, String parentID, String title) {
        this.categoryID = categoryID;
        this.parentID = parentID;
        this.title = title;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
