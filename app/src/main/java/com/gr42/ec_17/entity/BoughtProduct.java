package com.gr42.ec_17.entity;

public class BoughtProduct extends Product {
    private int boughtQuantity;

    public BoughtProduct(String productID, String title, float price, float avgRating, int quantityAvailable, int boughtQuantity) {
        super(productID, title, price, avgRating, quantityAvailable);
        this.boughtQuantity = boughtQuantity;
    }

    public int getBoughtQuantity() {
        return boughtQuantity;
    }

    public void setBoughtQuantity(int boughtQuantity) {
        this.boughtQuantity = boughtQuantity;
    }
}
