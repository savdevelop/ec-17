package com.gr42.ec_17.entity;

import androidx.annotation.NonNull;

public class Address {
    private String addressID = null, title, name, lastName, city, street;
    private Country country;

    public Address (){

    }

    public Address(String addressID, String title, String name, String lastName, String city, String street, Country country) {
        this.addressID = addressID;
        this.title = title;
        this.name = name;
        this.lastName = lastName;
        this.city = city;
        this.street = street;
        this.country = country;
    }

    public String getAddressID() {
        return addressID;
    }

    public void setAddressID(String addressID) {
        this.addressID = addressID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @NonNull
    @Override
    public String toString() {
        return street + ", " + city + ", " + country.getName() + ". " + name + " " + lastName + ".";
    }
}
