package com.gr42.ec_17.entity;

public class Country {
    private String countryID;
    private String name;
    private float shippingCost;

    public Country(String countryID, String name, float shippingCost) {
        this.countryID = countryID;
        this.name = name;
        this.shippingCost = shippingCost;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(float shippingCost) {
        this.shippingCost = shippingCost;
    }
}
