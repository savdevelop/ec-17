package com.gr42.ec_17.entity;

public class Product {
    private String productID, title, description;
    private float price, avgRating;
    private int quantityAvailable;
    private Review[] reviews;
    private String[] imageURLs;
    private Category category;

    public Product(String productID, String title, float price, float avgRating, int quantityAvailable) {
        this.productID = productID;
        this.title = title;
        this.price = price;
        this.avgRating = avgRating;
        this.quantityAvailable = quantityAvailable;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public Review[] getReviews() {
        return reviews;
    }

    public void setReviews(Review[] reviews) {
        this.reviews = reviews;
    }

    public String[] getImageURLs() {
        return imageURLs;
    }

    public void setImageURLs(String[] imageURL) {
        this.imageURLs = imageURL;
    }
}
