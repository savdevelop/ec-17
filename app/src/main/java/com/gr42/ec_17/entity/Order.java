package com.gr42.ec_17.entity;

import java.util.Calendar;

public class Order {
    private String orderID;
    private BoughtProduct[] products;
    private String address;
    private Calendar checkoutDate;

    public Order(String orderID, BoughtProduct[] products, String address, Calendar checkoutDate) {
        this.orderID = orderID;
        this.products = products;
        this.address = address;
        this.checkoutDate = checkoutDate;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public BoughtProduct[] getProducts() {
        return products;
    }

    public void setProducts(BoughtProduct[] products) {
        this.products = products;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Calendar getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(Calendar checkoutDate) {
        this.checkoutDate = checkoutDate;
    }
}
