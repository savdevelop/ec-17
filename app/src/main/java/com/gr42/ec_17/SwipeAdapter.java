package com.gr42.ec_17;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

public class SwipeAdapter extends PagerAdapter {
    private Context context;
    private String[] urls;
    private LayoutInflater layoutInflater;
    private int itemLayout;

    public SwipeAdapter(Context c, String[] urls, int itemLayout) {
        this.urls = urls;
        context = c;
        this.itemLayout = itemLayout;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (urls != null)
            count = urls.length;
        return count;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item = layoutInflater.inflate(itemLayout, container, false);
        ImageView image = item.findViewById(R.id.image);
        if (urls != null && urls.length > 0)
            Picasso.get().load(urls[position]).into(image);
        container.addView(item);
        return item;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
