package com.gr42.ec_17.control;

import androidx.annotation.Nullable;

import com.gr42.ec_17.entity.Product;
import com.parse.ParseObject;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BackendInterfaceTest {

    class ProductTest extends Product{
        public ProductTest(String productID, String title, float price, float avgRating, int quantityAvailable) {
            super(productID, title, price, avgRating, quantityAvailable);
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            Product newProduct = (Product) obj;
            return this.getProductID().equals(newProduct.getProductID());
        }
    }

    @Test
    public void parseObjectToProduct() {
        ParseObject input = ParseObject.createWithoutData("PRODUCT", "test");
        input.put("name", "name");
        input.put("price", 30.0);
        input.put("avgRating", 3.0);
        input.put("availablePieces", 1);
        List<ParseObject> list = new ArrayList<>();
        input.put("images", list);
        input.put("reviews", list);

        Product output;
        ProductTest expected;
        expected = new ProductTest("test", "name", (float)30.0, (float)3.0, 1);
        BackendInterface backendInterface = new BackendInterface();
        output = backendInterface.parseObjectToProduct(input);

        assertEquals(expected, output);
    }
}